package views;

import model.Polynomial;
import utilities.Parse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class PolynomialView extends JFrame {

    private JTextField poly1 = new JTextField();
    private JTextField poly2 = new JTextField();
    private JTextArea poly3 = new JTextArea();
    private JTextArea remainder = new JTextArea();
    private JLabel mainLabel = new JLabel("Welcome student!", SwingConstants.CENTER);
    private JButton clear = new JButton("Clear");
    private JButton add = new JButton("+");
    private JButton subtract = new JButton("-");
    private JButton multiply = new JButton("×");
    private JButton divide = new JButton("÷");
    private JButton integrate = new JButton("∫");
    private JButton differentiate;

    {
        differentiate = new JButton("∂");
    }

    public PolynomialView() {

        JPanel polyPanel = new JPanel();
        polyPanel.setLayout(new GridLayout(0,4));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000, 840);

        polyPanel.add(poly1);
        polyPanel.add(poly2);
        polyPanel.add(poly3);
        polyPanel.add(remainder);
        polyPanel.add(add);
        polyPanel.add(subtract);
        polyPanel.add(multiply);
        polyPanel.add(divide);
        polyPanel.add(integrate);
        polyPanel.add(differentiate);
        polyPanel.add(clear);
        polyPanel.add(mainLabel);
        this.add(polyPanel);
    }

    public String getFirstPolynomial() {
        return poly1.getText();
    }

    public String getSecondPolynomial() {
        return poly2.getText();
    }


    public void setPoly3(Polynomial result) {
        poly3.setText(result.toString());
    }

    public void setRemainder(Polynomial result) {
        remainder.setText("Remainder is:" + result.toString());
    }

    public void setMainLabel(String text) {
        this.mainLabel.setText(text);
    }

    public void clear(){
        poly1.setText("");
        poly2.setText("");
        poly3.setText("");
        remainder.setText("");
    }

    public void addClearListener(ActionListener listenForClearButton){
        clear.addActionListener(listenForClearButton);
    }
    public void addAddListener(ActionListener listenForAddButton){
        add.addActionListener(listenForAddButton);
    }
    public void addSubtractListener(ActionListener listenForSubtractButton){
        subtract.addActionListener(listenForSubtractButton);
    }
    public void addMultiplyListener(ActionListener listenForMultiplyButton){
        multiply.addActionListener(listenForMultiplyButton);
    }
    public void addDivideListener(ActionListener listenForDivideButton){
        divide.addActionListener(listenForDivideButton);
    }
    public void addIntegrateListener(ActionListener listenForIntegrateButton){
        integrate.addActionListener(listenForIntegrateButton);
    }
    public void addDifferentiateListener(ActionListener listenForDifferentiateButton){
        differentiate.addActionListener(listenForDifferentiateButton);
    }

}
