package model;

//import org.junit.Test;

public class PolynomialTest {
/*
	@Test
	public void testAddition() {
		Polynomial polynomial = new Polynomial();
		Polynomial polynomial1 = new Polynomial();
		Polynomial polynomial2 = new Polynomial();
		
		polynomial.add(new IntegerMonomial(2,3));
		polynomial.add(new IntegerMonomial(4,4));
		
		polynomial1.add(new IntegerMonomial(2,3));
		polynomial1.add(new IntegerMonomial(4,4));
		polynomial1.add(new IntegerMonomial(5,6));
		polynomial1.add(new IntegerMonomial(1,2));
		
		polynomial2.add(new IntegerMonomial(5,6));
		polynomial2.add(new IntegerMonomial(1,2));

		Polynomial polynomial3 = polynomial.add(polynomial2);
		assert(polynomial1.equals(polynomial3));
	}
	
	@Test
	public void testSubtraction() {
		Polynomial polynomial = new Polynomial();
		Polynomial polynomial1 = new Polynomial();
		Polynomial polynomial2 = new Polynomial();
		
		polynomial.add(new IntegerMonomial(2,3));
		polynomial.add(new IntegerMonomial(4,4));
		
		polynomial1.add(new IntegerMonomial(2,3));
		polynomial1.add(new IntegerMonomial(4,4));
		polynomial1.add(new IntegerMonomial(5,6));
		polynomial1.add(new IntegerMonomial(1,2));
		
		polynomial2.add(new IntegerMonomial(5,6));
		polynomial2.add(new IntegerMonomial(1,2));

		Polynomial polynomial3 = polynomial1.subtract(polynomial2);
		assert(polynomial.equals(polynomial3));
	}
	
	@Test
	public void testMultiplication() {
		Polynomial polynomial = new Polynomial();
		Polynomial polynomial1 = new Polynomial();
		Polynomial polynomial2 = new Polynomial();
		
		polynomial.add(new IntegerMonomial(2,3));
		polynomial.add(new IntegerMonomial(4,4));
		
		polynomial1.add(new IntegerMonomial(3,6));
		polynomial1.add(new IntegerMonomial(5,8));
		
		polynomial2.add(new IntegerMonomial(1,2));

		Polynomial polynomial3 = polynomial.multiply(polynomial2);
		assert(polynomial1.equals(polynomial3));
	}
	
	@Test
	public void testDivision() {
		
		//////////Division 1
		
		Polynomial divisor = new Polynomial();
		Polynomial dividend = new Polynomial();
		Polynomial quotient = new Polynomial();
		Polynomial rest = new Polynomial();
		
		divisor.add(new IntegerMonomial(3,3));
		divisor.add(new IntegerMonomial(0,2));
		
		dividend.add(new IntegerMonomial(2,1));
		dividend.add(new IntegerMonomial(1,-3));
		
		quotient.add(new IntegerMonomial(1,3));
		quotient.add(new IntegerMonomial(0,9));
		
		rest.add(new IntegerMonomial(1,27));
		rest.add(new IntegerMonomial(0,2));

		Polynomial polynomial1 = divisor.LongDivide(dividend)[0];
		Polynomial polynomial2 = divisor.LongDivide(dividend)[1];
		assert(quotient.equals(polynomial1) && rest.equals(polynomial2));
		
		//////////Division 2
		
		Polynomial divisor1 = new Polynomial();
		Polynomial dividend1 = new Polynomial();
		Polynomial quotient1 = new Polynomial();
		Polynomial rest1 = new Polynomial();
		
		divisor1.add(new IntegerMonomial(2,2));
		divisor1.add(new IntegerMonomial(0,1));
		
		dividend1.add(new IntegerMonomial(1,1));
		dividend1.add(new IntegerMonomial(0,1));
		
		quotient1.add(new IntegerMonomial(1,2));
		quotient1.add(new IntegerMonomial(0,-2));
		
		rest1.add(new IntegerMonomial(0,3));

		Polynomial polynomial11 = divisor1.LongDivide(dividend1)[0];
		Polynomial polynomial21 = divisor1.LongDivide(dividend1)[1];
		assert(quotient1.equals(polynomial11) && rest1.equals(polynomial21));
		
	}

	@Test
	public void testDifferentiation() {
		Polynomial polynomial = new Polynomial();
		Polynomial polynomial1 = new Polynomial();
		Polynomial polynomial2 = new Polynomial();
		Polynomial polynomial3 = new Polynomial();
		
		polynomial1.add(new IntegerMonomial(2,3));
		polynomial1.add(new IntegerMonomial(4,4));
		polynomial1.add(new IntegerMonomial(5,6));
		polynomial1.add(new IntegerMonomial(1,2));
		
		polynomial2.add(new IntegerMonomial(5,6));
		polynomial2.add(new IntegerMonomial(1,2));
		
		polynomial3.add(new IntegerMonomial(1,6));
		polynomial3.add(new IntegerMonomial(3,16));
		polynomial3.add(new IntegerMonomial(4,30));
		polynomial3.add(new IntegerMonomial(0,2));
		
		polynomial.add(new IntegerMonomial(4,30));
		polynomial.add(new IntegerMonomial(0,2));
		
		assert(polynomial3.equals(polynomial1.differentiate()));
		assert(polynomial.equals(polynomial2.differentiate()));
	}
	
	@Test
	public void testIntegration() {
		Polynomial polynomial = new Polynomial();
		Polynomial polynomial1 = new Polynomial();
		Polynomial polynomial2 = new Polynomial();
		Polynomial polynomial3 = new Polynomial();
		
		polynomial1.add(new IntegerMonomial(2,3));
		polynomial1.add(new IntegerMonomial(4,4));
		polynomial1.add(new IntegerMonomial(5,6));
		polynomial1.add(new IntegerMonomial(1,2));
		
		polynomial2.add(new IntegerMonomial(5,6));
		polynomial2.add(new IntegerMonomial(1,2));
		
		polynomial3.add(new IntegerMonomial(1,6));
		polynomial3.add(new IntegerMonomial(3,16));
		polynomial3.add(new IntegerMonomial(4,30));
		polynomial3.add(new IntegerMonomial(0,2));
		
		polynomial.add(new IntegerMonomial(4,30));
		polynomial.add(new IntegerMonomial(0,2));
		
		assert(polynomial1.equals(polynomial3.integrate()));
		assert(polynomial2.equals(polynomial.integrate()));
	}
	*/
}
