package controllers;

import exceptions.InvalidInputException;
import model.Polynomial;
import utilities.Parse;
import views.PolynomialView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PolynomialController {

    private PolynomialView theView;
    private Polynomial polynomial1;
    private Polynomial polynomial2;

    public PolynomialController(Polynomial polynomial1, Polynomial polynomial2, PolynomialView theView){
        this.theView = theView;
        this.polynomial1 = polynomial1;
        this.polynomial2 = polynomial2;
        this.theView.addAddListener(new AddListener());
        this.theView.addSubtractListener(new SubtractListener());
        this.theView.addMultiplyListener(new MultiplyListener());
        this.theView.addDivideListener(new DivideListener());
        this.theView.addIntegrateListener(new IntegrateListener());
        this.theView.addDifferentiateListener(new DifferentiateListener());
        this.theView.addClearListener(new ClearListener());
    }

    public class ClearListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            theView.clear();
            polynomial1 = new Polynomial();
            polynomial2 = new Polynomial();
        }
    }
    public class AddListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                polynomial1 = Parse.parsePolynomial(theView.getFirstPolynomial());
                polynomial2 = Parse.parsePolynomial(theView.getSecondPolynomial());
                theView.setPoly3(polynomial1.add(polynomial2));
                theView.setMainLabel("Successful addition!");
            } catch (InvalidInputException ex){
                theView.setMainLabel("Invalid input! The format should be: coef*X^degree.");
            }

        }
    }
    public class SubtractListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                polynomial1 = Parse.parsePolynomial(theView.getFirstPolynomial());
                polynomial2 = Parse.parsePolynomial(theView.getSecondPolynomial());
                theView.setPoly3(polynomial1.subtract(polynomial2));
                theView.setMainLabel("Successful subtraction!");
            } catch (InvalidInputException ex){
                theView.setMainLabel("Invalid input! The format should be: coef*X^degree.");
            }

        }
    }
    public class MultiplyListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                polynomial1 = Parse.parsePolynomial(theView.getFirstPolynomial());
                polynomial2 = Parse.parsePolynomial(theView.getSecondPolynomial());
                theView.setPoly3(polynomial1.multiply(polynomial2));
                theView.setMainLabel("Successful multiplication!");
            } catch (InvalidInputException ex){
                theView.setMainLabel("Invalid input! The format should be: coef*X^degree.");
            }
        }
    }
    public class DivideListener implements ActionListener { 
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                polynomial1 = Parse.parsePolynomial(theView.getFirstPolynomial());
                polynomial2 = Parse.parsePolynomial(theView.getSecondPolynomial());
                theView.setPoly3(polynomial1.LongDivide(polynomial2)[0]);
                theView.setRemainder(polynomial1.LongDivide(polynomial2)[1]);
                theView.setMainLabel("Successful division!");
            } catch (InvalidInputException ex){
                theView.setMainLabel("Invalid input! The format should be: coef*X^degree.");
            }
        }
    }
    public class IntegrateListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                polynomial1 = Parse.parsePolynomial(theView.getFirstPolynomial());
                theView.setPoly3(polynomial1.integrate());
                theView.setMainLabel("Successful integration!");
            } catch (InvalidInputException ex){
                theView.setMainLabel("Invalid input! The format should be: coef*X^degree.");
            }
        }
    }
    public class DifferentiateListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                polynomial1 = Parse.parsePolynomial(theView.getFirstPolynomial());
                theView.setPoly3(polynomial1.differentiate());
                theView.setMainLabel("Successful derivation!");
            } catch (InvalidInputException ex){
                theView.setMainLabel("Invalid input! The format should be: coef*X^degree.");
            }

        }
    }
}
