package exceptions;

/**
 * Created by John on 13.03.2017.
 */
public class InvalidInputException extends Exception {
    public InvalidInputException() {
        System.out.println("Invalid input!");
    }
}
